
package day01randomsali;

import java.util.InputMismatchException;
import java.util.Scanner;

/* @author alagh */
public class Day01RandomsAli {
    
    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        try {
            System.out.println("How many random integers do you want to generate");
            int count = input.nextInt(); // ex IME
            if (count < 0) {
                System.out.println("Error: Number must not be negative");
                System.exit(1);
            }
            System.out.print("Enter minimum: \n");
            int min = input.nextInt(); // ex Input Mismatch Exception (IME)
            System.out.print("Enter maximum: \n");
            int max = input.nextInt(); // ex IME
            if (min > max) {
                System.out.println("Error: Maximum must be larger or equal to minimum");
                System.exit(1);
            }
            System.out.print("Result: ");
            for (int i = 0; i < count; i++) {
                int val = (int)(Math.random()*(max - min + 1) + min);
                System.out.printf("%s%d", (i == 0 ? "": ", "), val);
            }
            System.out.println();
            }
        catch (InputMismatchException ex) {
            System.out.println("Error: value must be an integer.");
            }
        }
    
}
