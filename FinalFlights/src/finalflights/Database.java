/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalflights;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.sql.SQLException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.locks.StampedLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alagh
 */
public class Database {

    private static final String dbURL = "jdbc:mysql://localhost:3306/finaldb";
    private static final String username = "root";
    private static final String password = "root2021";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }

    public ArrayList<Flight> getAllFlights() throws SQLException {
        try {
            ArrayList<Flight> list = new ArrayList<>();
            String sql = "SELECT id,onDate, fromCode, toCode, type, passengers FROM flights";
            PreparedStatement statement = conn.prepareStatement(sql);
            // it is a good practice to use try-with-resources for ResultSet so it is freed up as soon as possible
            try (ResultSet result = statement.executeQuery(sql)) {
                while (result.next()) { // has next row to read
                    int id = result.getInt("id");
                    Date onDate = result.getDate("onDate"); // parse date using simple date format
                    String fromCode = result.getString("fromCode");
                    String toCode = result.getString("toCode");
                    String typeStr = result.getString("type"); // enum parsed (todo)
                    Flight.Type type = Flight.Type.valueOf(typeStr);
                    int passengers = result.getInt("passengers");
                    list.add(new Flight(id, onDate, fromCode, toCode, type, passengers));
                }
                return list;
            }
        } catch (IllegalArgumentException ex) {
            throw new SQLException("Error creating todo from record", ex); // exception chaining
        }
    }
    
    public void addFlight(Flight flight) throws SQLException {
        String sql = "INSERT INTO flights VALUES (NULL, ?, ?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setDate(1, (java.sql.Date) flight.onDate);
        statement.setString(2, flight.fromCode);
        statement.setString(3, flight.toCode);
        statement.setString(4, flight.getType() + "");
        statement.setInt(5, flight.passengers);
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted");
    }

}
