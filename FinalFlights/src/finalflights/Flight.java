/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalflights;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Ali Alaghbandrad
 */
public class Flight {
    public Flight(int id, Date onDate, String fromCode, String toCode, Type type, int passengers) {
        this.id = id;
        this.onDate = onDate;
        this.fromCode = fromCode;
        this.toCode = toCode;
        // this.type = type;
        setType(type);
        this.passengers = passengers;
    }
    
    int id;
    Date onDate;
    String fromCode; 
    String toCode;
    Type type;
    int passengers;
    
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        return String.format("Flight %d On %s from %s to %s has %d passengers", id, dateFormatDisplay.format(onDate), fromCode, toCode, passengers);
    }
    
    static SimpleDateFormat dateFormatDisplay = new SimpleDateFormat("yyyy-MM-dd");
    
    enum Type {
        Domestic, International, Private
    }
}
