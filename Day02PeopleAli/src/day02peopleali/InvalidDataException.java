
package day02peopleali;

/* @author alagh */
public class InvalidDataException extends Exception {
    public InvalidDataException(String msg) {
        super(msg);
    }
}
