
package java3midterm;

/* @author Ali Alaghbandrad */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.exit;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Arrays.sort;
import static java.util.Arrays.sort;
import java.util.Collections;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java3midterm.IceCreamOrder.Flavour;

public class Java3Midterm {


    static ArrayList<IceCreamOrder> ordersList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    
    static int inputInt() {
        while (true) {
            try {
                int result = input.nextInt(); // ex InputMismatchException
                input.nextLine(); // consume the leftover newline
                return result;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid input, enter an integer. Try again.");
                input.nextLine(); // consume the invalid input
            }
        }
    }

    static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add an order\n"
                + "2. List orders by customer name\n"
                + "3. List orders by delivery date\n"
                + "0. Exit\n"
                + "Please enter your choice: ");
        int choice = inputInt();
        return choice;
    }
    
    public static void main(String[] args) {
        loadDataFromFile();
        while (true) {
            System.out.println("Current instance count: " + IceCreamOrder.getInstanceCount());
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    addIceCreamOrder();
                    break;
                case 2:
                    listAllOrdersByName();
                    break;
                case 3:
                    listAllOrderByDeliveryDate();
                    break;
                case 0:
                    saveDataToFile();
                    System.out.println("Exiting.");
                    return;
                default:
                    System.out.println("Invalid choice, try again");
            }
            System.out.println();
        }
    }
    
    private static void addIceCreamOrder() {
        try {
            System.out.println("Adding an Icecream order.");
            System.out.print("Please enter customer's name: ");
            String customerName = input.nextLine();
            System.out.print("Enter the delivery date (yyyy-mm-dd): ");
            String delDateStr = input.nextLine();
            Date delDate = Globals.dateFormatScreen.parse(delDateStr); // ex ParseException
            System.out.print("Enter delivery time (hh:mm): ");
            String delTimeStr = input.nextLine();
            Date delTime = Globals.dateFormatScreen.parse(delTimeStr); // ex ParseException
            ArrayList<String> flavour = new ArrayList<String>();
            Scanner fl = new Scanner(System.in);
            String flavourString = "";
            while (true) {
                System.out.print("Enter which flavour to add, empty to finish: ");
                flavour.add(fl.next());
                if ("".equals(flavour)) {
                    break;
                };
                // if (flavour != "VANILLA" | "CHOCOLATE" | "STRAWBERRY" | "ROCKYROAD") {
                //    System.out.print("No such flavour. Try again.\n");
                // }
            }
            
            for (String f : flavour)
            {
                flavourString += f + "\t";
            }

            System.out.println(flavourString);
            IceCreamOrder iceCreamOrder = new IceCreamOrder(customerName, delDate, delTime, flavourString); // ex DataInvalidException
            ordersList.add(iceCreamOrder);
            System.out.println("Order accepted:");
            System.out.println(iceCreamOrder);
        } catch (ParseException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        }
    }
    
    private static void listAllOrdersByName() {
        if (ordersList.isEmpty()) {
            System.out.println("No order found");
            return;
        }
        Collections.sort(ordersList);
        for (int i = 0; i < ordersList.size(); i++) {
            System.out.printf("#%d: %s\n", i + 1, ordersList.get(i));
        }
    }
    
        private static void listAllOrderByDeliveryDate() {
        if (ordersList.isEmpty()) {
            System.out.println("No order found");
            return;
        }
        for (int i = 0; i < ordersList.size(); i++) {
            System.out.printf("#%d: %s\n", i + 1, ordersList.get(i));
        }
    }
    
    
    final static String DATA_FILE_NAME = "orders.txt";
    
    static void loadDataFromFile() { 
        try (Scanner inputFile = new Scanner(new File(DATA_FILE_NAME))) {
            while (inputFile.hasNextLine()) {
                String line = "";
                line = inputFile.nextLine();
                ordersList.add(new IceCreamOrder(line)); // ex DataInvalidException
            }
        } catch (FileNotFoundException ex) {
            // ignore, it's okay if the file is not there
        }
    }
    
    static void saveDataToFile() { // calls todo.toDataString();
        try (PrintWriter outputFile = new PrintWriter(new File(DATA_FILE_NAME))) {
            for (IceCreamOrder iceCreamOrder : ordersList) {
                outputFile.println(iceCreamOrder.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing data back to file");
        }
    }

    
}
