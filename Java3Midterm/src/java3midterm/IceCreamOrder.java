
package java3midterm;

/* @author Ali Alaghbandrad */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.IntStream;
import java3midterm.IceCreamOrder.Flavour;

public class IceCreamOrder {

    IceCreamOrder(String line) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public enum Flavour { VANILLA, CHOCOLATE, STRAWBERRY, ROCKYROAD };
    
    ArrayList<Flavour> flavList = new ArrayList<Flavour>();
    
    public IceCreamOrder(String customerName, Date delDate, Date delTime, flavourString) throws DataInvalidException {
        setName(customerName);
        setDelDate(delDate);
        setDelTime(delTime);
        setFlavour(flavour);
    }
    
    public IceCreamOrder(String dataLine) throws DataInvalidException { // throws ParseException, NumberFormatException, IllegalArgumentException {
        String[] data = dataLine.split(";");
        if (data.length != 4) {
            throw new DataInvalidException("Invalid number of items in line");
        }
        try {
            
            String name = data[0];
            Date delDate = dateFormatFile.parse(data[1]); // ex ParseException
            setName(customerName); // ex DataInvalidException
            setDelDate(delDate); // ex DataInvalidException

        } catch (ParseException ex) {
            throw new DataInvalidException("Date format invalid", ex);
        } catch (NumberFormatException ex) {
            throw new DataInvalidException("Integer value expected", ex);
        }
    }
    
    private static int instanceCount; // static, read-only property

    public static int getInstanceCount() {
        return instanceCount;
    }
    
    private String customerName; // 2-50 characters long, must NOT contain a semicolon or | or ` (reverse single quote) characters
    private Date delDate; // Date between year 1900 and 2100



    
    
    @Override
    public String toString() {
        String delDateStr = Globals.dateFormatScreen.format(delDate);
        String delTimeStr = Globals.dateFormatScreen.format(delTime);
        return String.format("%s, %s, %s, %s", customerName, delDateStr, delTimeStr, flavourString);
    }
    
    private static final SimpleDateFormat dateFormatFile;
    
    static { // static initializer
        dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatFile.setLenient(false);
    }
    
    public String toDataString() {
        
        String delDateStr = dateFormatFile.format(delDate);
        return String.format("%s;%s;%s;%s", customerName, delDateStr, delTimeStr, flavourString);
    }
    
    public String getName() {
        return customerName;
    }

    public void setName(String customerName) throws DataInvalidException {
        //if (customerName.length() < 2 || name.length() > 50) {
        if (!customerName.matches("[^;|`]{2,50}")) {
            throw new DataInvalidException("Name must be 2-50 character long and not contain ;|` characters");
        }
        this.customerName = customerName;
    }
    
    public Date getDelDate(Date delDateStr) {
        return delDateStr;
    }

    public void setDelDate(Date delDate) throws DataInvalidException {
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(delDate);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 || year > 2100) { // error
            throw new DataInvalidException("Year must be 1900 to 2100");
        }
        this.delDate = delDate;
    }
    
}
