
package java3midterm;

/* @author Ali Alaghbandrad */

import java.text.SimpleDateFormat;

public class Globals {
    
    static final SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd");
    
    static { // static initializer
        dateFormatScreen.setLenient(false);
    }
}
