
package day02todosali;

import java.text.SimpleDateFormat;

/* @author alagh */
public class Globals {
    // TODO: use toLocalizedPattern()
    static final SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd");
    
    static { // static initializer
        dateFormatScreen.setLenient(false);
    }
}
